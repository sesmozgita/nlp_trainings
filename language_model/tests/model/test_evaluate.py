from language_model.model.evaluate import SeqLossCounter
import torch
import torch.nn as nn
import numpy as np
import numpy.testing as npt
import unittest
torch.manual_seed(42)
np.random.seed(42)


class Model(nn.Module):
    def __init__(self, n_voc, embed_dim):
        super(Model, self).__init__()
        self.emb = nn.Embedding(n_voc, embed_dim)
        self.lin = nn.Linear(embed_dim, n_voc)

    def forward(self, x):
        emb = self.emb(x)
        return self.lin(emb)


batch_size, len_sent, emb_dim, n_vocab = 5, 10, 12, 100
data = torch.randint(low=0, high=n_vocab, size=(batch_size, len_sent))
model = Model(n_voc=n_vocab, embed_dim=emb_dim)


class MyTestCase(unittest.TestCase):
    def test_seqlosscounter(self):
        loss = SeqLossCounter(model)
        error = loss.compute_loss(data)
        self.assertAlmostEqual(error, 4.3063, delta=0.01)

    def test_extract_target_prob(self):
        loss = SeqLossCounter(model)
        inp = torch.tensor([[[0.1, 0.2, 0.6, 0.1],
                            [0.8, 0.05, 0.06, 0.09],
                            [0.3, 0.2, 0.4, 0.1]],
                           [[0.6, 0.2, 0.1, 0.1],
                            [0.8, 0.1, 0.06, 0.04],
                            [0.5, 0.2, 0.2, 0.1]]],
                           dtype=torch.float32)
        trg = torch.tensor([[2, 0, 1], [0, 0, 1]], dtype=torch.int64).unsqueeze(2)
        result = loss.extract_target_prob(logits=inp, target=trg).tolist()
        prob_to_update = [[inp[0][0][trg[0][0]], inp[0][1][trg[0][1]], inp[0][2][trg[0][2]]],
                         [inp[1][0][trg[1][0]], inp[1][1][trg[1][1]], inp[1][2][trg[1][2]]]]
        self.assertListEqual(prob_to_update[0], result[0])

    def test_cross_entropy_loss(self):
        arr = np.random.randint(0, 205, size=(3, 10, 205))
        def softmax(array):
            array = np.array(array)
            return np.exp(array)/np.sum(np.exp(array), axis=-1)[:, :, None]
        x = torch.tensor(softmax(arr), dtype=torch.float32)
        y = np.array([[151, 138, 23, 110, 3, 56, 41, 35, 158, 162],
                      [26, 94, 184, 184, 120, 68, 156, 13, 75, 154],
                      [2, 64, 82, 96, 173, 108, 64, 68, 96, 148]])
        npt.assert_array_equal(y, x.argmax(-1))

        mask = torch.ones(size=(y.shape[0], y.shape[1]+1))
        loss = SeqLossCounter(model)
        result = loss.cross_entropy_loss(x, torch.tensor(y, dtype=torch.int64)[:, :, None], mask)

        def cross_entropy(yhat, y):
            return - np.mean(np.sum(y * np.log(yhat), axis=0))
        x = softmax(arr)
        x_after_model = np.take_along_axis(x, x.argmax(-1)[:, :, None], axis=-1).reshape(y.shape)
        manual_result = cross_entropy(x_after_model, np.ones(y.shape))
        self.assertAlmostEqual(manual_result, result, delta=1e-3)


if __name__ == '__main__':
    unittest.main()
