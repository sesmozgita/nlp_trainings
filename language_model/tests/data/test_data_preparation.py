import unittest
import numpy as np
from language_model.data.data_preparation import tokens_extract, vocabulary_making, data_for_model_preparing, \
    batch_maker, batch_generator
np.random.seed(42)


class MyTestCase(unittest.TestCase):
    def test_batch_generator(self):
        max_len = 20
        batch_size = 5
        data = [np.random.randint(0, 100, size=np.random.randint(20)) for _ in range(100)]
        batches = list(next(batch_generator(data, batch_size=batch_size, max_len=max_len, pad=-10)))
        len_batch = [len(x) for x in batches]
        self.assertEqual(len(batches), batch_size)
        self.assertEqual(sum(len_batch)/len(batches), max_len)


if __name__ == '__main__':
    unittest.main()
