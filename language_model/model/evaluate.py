import torch
import torch.nn.functional as F
from language_model.configs.vocab_configs import EOS_IND


class SeqLossCounter:
    def __init__(self, model):
        self.model = model

    def softmax(self, arr):
        return torch.exp(arr) / torch.exp(arr).sum(-1)[:, :, None]

    def target_extract(self, data):
        input_ix = torch.as_tensor(data, dtype=torch.int64)
        target = input_ix[:, 1:]  # [batch_size, len_seq-1]
        return target

    def source_extract(self, data):
        input_ix = torch.as_tensor(data, dtype=torch.int64)
        src = input_ix[:, :-1]  # [batch_size, len_seq-1]
        return src

    def model_predict(self, data):
        logits = self.model(data)  # [batch_size, len_seq-1, n_tokens]
        logits = self.softmax(logits)  # [batch_size, len_seq-1, n_tokens]
        return logits

    def extract_target_prob(self, logits, target):
        """
        model return probability distribution of targets on each seq position. Due to be able to count
        cross entropy loss it is need to extract from logits only probability on target position
        logits: [batch_size, len_seq-1, n_tokens] prediction of model with probability distribution
        target: [batch_size, len_seq-1] tensor with correct index of target position
        """
        out = logits.gather(2, target).squeeze(0).squeeze(-1)
        return out

    def cross_entropy_loss(self, x: torch.tensor, y: torch.tensor, mask: torch.tensor):
        """
        :param x: current text (t-1 words) [batch_size, len_seq-1, n_tokens]
        :param y: next word (t word) [batch_size, len_seq-1]
        :param mask: mask to prevent of calculating loss on pad token
        :return:
        """
        logits_update = self.extract_target_prob(x, y)  # [batch_size, len_seq-1]
        loss = -1 * torch.log(logits_update)
        loss *= mask[:, 1:]
        loss = torch.mean(torch.sum(loss, dim=0))
        return loss

    def compute_loss(self, data: torch.tensor):
        target = self.target_extract(data).unsqueeze(2)  # [batch_size, len_seq-1, 1]
        src = self.source_extract(data)  # [batch_size, len_seq-1]
        logits = self.model_predict(src)  # [batch_size, len_seq-1, n_tokens]
        mask = F.pad(torch.cumsum(data == EOS_IND, dim=-1)[:, :-1] < 1,
                     pad=(1, 0, 0, 0), value=True)  # [batch_size, len_seq-1]
        # match
        loss = self.cross_entropy_loss(logits, target, mask)
        return loss