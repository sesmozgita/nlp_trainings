import torch.nn as nn
import torch
import numpy as np
from utils import logger_setup
from language_model.configs.vocab_configs import UNK_IND, BOS, EOS
logger = logger_setup('./logs/train_model.log')


class LM(nn.Module):
    def __init__(self, n_tokens: int, emb_size: int, hid_size_conv: int, hid_size_gru: int, stride: int = 1,
                 kernel_size: int = 3, n_gru_directions: bool = True):
        super().__init__()
        # logger.info("Initialize LM model")
        # https://towardsdatascience.com/how-to-build-a-gated-convolutional-neural-network-gcnn-for-natural-language-processing-nlp-5ba3ee730bfb
        self.stride = stride
        self.kernel_size = kernel_size
        self.padding = 0
        self.emb = nn.Embedding(n_tokens, emb_size)  # [batch_size, len_seq, emb_size ]
        self.conv1 = nn.Conv1d(emb_size, hid_size_conv, stride=self.stride, kernel_size=self.kernel_size,
                               padding=self.padding)
        self.conv2 = nn.Conv1d(emb_size, hid_size_conv, stride=self.stride, kernel_size=self.kernel_size,
                               padding=self.padding)
        self.conv3 = nn.Conv1d(emb_size, hid_size_conv, stride=self.stride, kernel_size=self.kernel_size,
                               padding=self.padding)
        self.len_pad = self.stride * (self.kernel_size - 1)
        self.pad = nn.ZeroPad2d((self.stride * (self.kernel_size - 1), 0, 0, 0))
        self.n_gru_directions = n_gru_directions
        self.gru = nn.GRU(hid_size_conv, hid_size_gru, batch_first=True, bidirectional=self.n_gru_directions)
        self.linear = nn.Linear(hid_size_gru*(1+int(self.n_gru_directions)), n_tokens)
        modules = list(self.modules())
        conv_layers = [layer for layer in modules if layer.__dict__.get("in_channels", 0) > 0]
        # logger.info(f"stacking of {len(conv_layers)} same conv layers with such parameters provide visibility "
        #             f"to {1+self.kernel_size*len(conv_layers)} context")

    def __call__(self, input_ix):
        emb = self.emb(input_ix)  # [batch_size, len_seq, emb_size ]
        emb = emb.permute(0, 2, 1)  # [batch_size, emb_size, len_seq ]

        padded = self.pad(emb)
        conv1 = self.conv1(padded).permute(0, 2, 1)  # [batch_size, len_seq, hid_size ]
        conv2 = self.conv2(padded).permute(0, 2, 1)  # [batch_size, len_seq, hid_size ]
        conv2 = torch.sigmoid(conv2)
        conv = torch.mul(conv1, conv2)  # [batch_size, len_seq, hid_size]
        output, hidden = self.gru(conv)  # [batch_size, len_seq, hid_size*n_directions ]
        out = self.linear(output)  # [batch_size, len_seq, n_tokens ]
        return out

    def get_possible_next_tokens(self, vocab: dict, prefix: str = BOS, temperature=1.0, max_len=100):
        prefix_ix = torch.as_tensor([[vocab.get(prefix, UNK_IND)]], dtype=torch.int64)
        with torch.no_grad():
            probs = torch.softmax(self(prefix_ix)[0, -1], dim=-1).numpy()  # shape: [n_tokens]
        return dict(zip(vocab, probs))

    def generate_sequences(self, vocab: dict, prefix: str = BOS, temperature=1.0, max_len=100):
        with torch.no_grad():
            while True:
                token_probs = self.get_possible_next_tokens(vocab, prefix)
                tokens, probs = zip(*token_probs.items())
                if temperature == 0:
                    next_token = tokens[np.argmax(probs)]
                else:
                    probs = np.array([p ** (1. / temperature) for p in probs])
                    probs /= sum(probs)
                    next_token = np.random.choice(tokens, p=probs)
                prefix += " " + next_token
                if next_token == EOS or len(prefix) > max_len:
                    break
        return prefix
