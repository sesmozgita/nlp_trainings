import torch
from torch.optim.lr_scheduler import ReduceLROnPlateau
from language_model.data.data_preparation import batch_generator
from language_model.model.evaluate import SeqLossCounter
from utils import logger_setup

logger = logger_setup("./logs/train_model.log")


def train_loop(model, optimizer, train_data, test_data, **batch_kwargs):
    # device = "cuda" if torch.cuda.is_available() else "cpu"
    device = "cpu"
    scheduler = ReduceLROnPlateau(optimizer, mode='min', factor=0.01, patience=1, threshold=0.1, verbose=True)
    loss = SeqLossCounter(model)
    best_score = None
    for epoch in range(20):
        model = model.to(device)
        model.train()
        t_history = []
        v_history = []
        for batch in batch_generator(data=train_data, **batch_kwargs):
            optimizer.zero_grad()
            loss_tr = loss.compute_loss(batch.to(device))
            loss_tr.backward()
            torch.nn.utils.clip_grad_norm(model.parameters(), 1)
            optimizer.step()
            t_history.append(loss_tr.item())

        for batch in batch_generator(data=test_data, **batch_kwargs):
            model.eval()
            loss_vld = loss.compute_loss(batch.to(device))
            v_history.append(loss_vld.item())
        scheduler.step(sum(v_history) / len(v_history))

        checkpoint = {
            'epoch': epoch + 1,
            'state_dict': model.state_dict(),
            'optimizer': optimizer.state_dict()
        }
        if best_score is None:
            best_score = sum(v_history)
        if sum(v_history) < best_score:
            acc = "".join(f"{round(sum(v_history) / len(v_history), 2)}".split('.'))
            torch.save(checkpoint, f'language_model_{acc}.pt')

        if epoch % 10 == 0:
            logger.info(
                f"Epoch: {epoch} | Mean loss train: {sum(t_history) / len(t_history)} | | Mean loss valid: {sum(v_history) / len(v_history)}")
        logger.info("I am end")
