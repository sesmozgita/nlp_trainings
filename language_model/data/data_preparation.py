from collections import Counter
from language_model.configs.vocab_configs import *
from typing import List, Dict, Tuple
import torch
import torch.nn.functional as F
import re
import numpy as np


def tokens_extract(data: List[str]) -> Dict[str, int]:
    tokens = Counter((" ".join(data).lower()).split(' '))
    return tokens


def vocabulary_making(data: List[str]) -> Dict[str, int]:
    tokens = tokens_extract(data)
    vocab: Dict[str, int] = dict()
    vocab[PAD] = PAD_IND
    vocab[UNK] = UNK_IND
    vocab[BOS] = BOS_IND
    vocab[EOS] = EOS_IND
    n = 4
    for w in tokens:
        if (w not in vocab) and (tokens[w] > N_MIN_TKN):
            vocab[w] = n
            n += 1
    return vocab


def data_for_model_preparing(data: List[str], vocab: Dict[str, int]) -> Tuple[List[List[int]], List[List[int]]]:
    unk_id = vocab[UNK]
    shfl_ind = np.arange(len(data))
    np.random.shuffle(shfl_ind)
    mix_data = (" ".join(np.array(data)[shfl_ind])).split(". ")
    sentences = []
    for txt in mix_data:
        sentences.extend([[vocab[BOS]] + [vocab.get(x.lower(), unk_id) for x in txt] + [vocab[EOS]]])
    length = len(sentences)
    tr_ind, tst_ind = np.arange(length)[:int(0.8 * length)], np.arange(length)[int(0.8 * length):]
    train_lines, dev_lines = np.array(sentences)[tr_ind], np.array(sentences)[tst_ind]
    return train_lines, dev_lines


def batch_maker(batch: List[List[int]], max_len: int = None, pad: int = PAD_IND):
    max_len = max_len if max_len else max([len(txt) for txt in batch])
    result = torch.empty(len(batch), max_len)
    for i, txt in enumerate(batch):
        txt = torch.tensor(txt, dtype=torch.int64)
        result[i] = F.pad(txt, (0, max_len - len(txt)), value=pad)
    return torch.tensor(result, dtype=torch.int64)


def batch_generator(data, batch_size, max_len: int = None, pad: int = PAD_IND, shuffle: str = False):
    if shuffle:
        shfl_ind = np.arange(len(data))
        np.random.shuffle(shfl_ind)
        data = data[shfl_ind]
    n_batches = len(data)//batch_size
    for i in range(n_batches):
        batch = data[i*batch_size: batch_size + i*batch_size]
        yield batch_maker(batch, max_len, pad)
