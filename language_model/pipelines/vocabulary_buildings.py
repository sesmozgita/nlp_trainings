from typing import List
from language_model.data.data_preparation import data_for_model_preparing, tokens_extract
import pandas as pd
import os


path = "./arxivData.json"
data: List[str] = pd.read(path).apply(lambda row: row['summary'], axis=1).apply(lambda line: line.replace('\n', ' ')).tolist()


if __name__=="__main__":
    vocab_tkn_id = tokens_extract(data)
    os.environ["N_TOKENS"] = len(vocab_tkn_id)
