import pandas as pd
import torch

from language_model.data.data_preparation import data_for_model_preparing
from language_model.data.data_preparation import vocabulary_making
from language_model.model.model import LM
from language_model.model.train_model import train_loop

emb_size = 15
hid_size_conv = 32
hid_size_gru = 128
batch_kwargs = {"batch_size": 256, "max_len": 40}

if __name__=="__main__":
    data = pd.read_json("./arxivData.json")["summary"].str.replace('\n', ' ').tolist()
    vocab = vocabulary_making(data)
    train_lines, dev_lines = data_for_model_preparing(data, vocab)[:1000]

    model = LM(n_tokens=len(vocab), emb_size=emb_size,
               hid_size_conv=hid_size_conv, hid_size_gru=hid_size_gru)
    optimizer = torch.optim.Adam(model.parameters())
    train_loop(model=model, optimizer=optimizer, train_data=train_lines, test_data=dev_lines, **batch_kwargs)