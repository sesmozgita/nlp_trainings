import numpy as np
from typing import List, Union, Iterable, Dict
import torch
import torch.nn.functional as F
import youtokentome as yttm
from translator.configs.vocab_configs import *
from nltk.tokenize import WordPunctTokenizer
from collections import Counter
from itertools import chain
from functools import partial
from utils import get_from_dict
import os


def add_spec(text):
    return [BOS + txt + EOS for txt in text]


class SimpleVocab:
    def __init__(self, bos_ind=0, eos_ind=1, unk_id=2, pad_ind=3):
        self.dict_word2ind = None
        self.dict_ind2word = None
        self.bos_ind, self.eos_ind, self.unk_id, self.pad_ind = bos_ind, eos_ind, unk_id, pad_ind

    def word2ind(self, word):
        return self.dict_word2ind.get(word, self.unk_id)

    def ind2word(self, word):
        return self.dict_ind2word.get(word, UNK)

    def tokenize(self, x):
        tokenizer = WordPunctTokenizer()
        return tokenizer.tokenize(x)

    def vocab_build(self, text: List[str], vocab_size: int):
        tokenized = [self.tokenize(x.lower()) for x in text]
        all_words = Counter(list(chain.from_iterable(tokenized)))
        vocab = all_words.most_common(vocab_size)
        self.dict_word2ind = {w: ind for ind, w in enumerate([BOS, EOS, UNK, PAD])}
        for ind, word_count in enumerate(vocab):
            word, counts = word_count
            if word not in self.dict_word2ind:
                self.dict_word2ind[word] = len(self.dict_word2ind)
        self.dict_ind2word = {ind: w for w, ind in self.dict_word2ind.items()}

    def encode(self, text: list):
        """
        convert each subword to id.
        :param text:
        :return:
        """
        assert (self.dict_word2ind is not None)
        tokenized: list = [self.tokenize(x) for x in text] + [self.eos_ind]
        return [[self.word2ind(word) for word in txt] for txt in tokenized]

    def decode(self, text: list):
        """
        convert each id to subword and concatenate with space symbol.
        :param text:
        :return:
        """
        return [" ".join([self.ind2word(word) for word in txt]) for txt in text]

    def to_matrix(self, lines, max_len=None):
        lines = self.encode(lines)
        max_len = max_len or max(map(len, lines))
        matrix = []
        for i, seq in enumerate(lines):
            row_ix = seq[:max_len]
            matrix.append(row_ix)
        return matrix

    def compute_mask(self, input_ix):
        """ compute a boolean mask that equals "1" until first EOS (including that EOS) """
        return F.pad(torch.cumsum(input_ix == self.eos_ind, dim=-1)[..., :-1] < 1, pad=(1, 0, 0, 0), value=True)

    def vocab_size(self):
        return len(self.dict_word2ind)

    def to_lines(self, matrix, crop=True):
        """
        Convert matrix of token ids into strings
        :param matrix: matrix of tokens of int32, shape=[batch,time]
        :param crop: if True, crops BOS and EOS from line
        :return:
        """
        lines = []
        for line_ix in map(list, matrix):
            if crop:
                if line_ix[0] == self.bos_ind:
                    line_ix = line_ix[1:]
                if self.eos_ind in line_ix:
                    line_ix = line_ix[:line_ix.index(self.eos_ind)]
            line = ' '.join(self.ind2word(i) for i in line_ix)
            lines.append(line)
        return lines


class YttmVocab(SimpleVocab):
    def __init__(self, bpe_path, bos_ind=0, eos_ind=1, unk_id=2, pad_ind=3):
        super().__init__(bos_ind, eos_ind, unk_id, pad_ind)
        self.bpe_path = bpe_path
        self.bpe = None
        self.bos_ind = bos_ind
        self.eos_ind = eos_ind
        self.unk_id = unk_id
        self.pad_ind = pad_ind

    def vocab_build(self, text: list, vocab_size: int, tmp_data_path: str = None):
        tokenized = [self.tokenize(x) for x in text]
        if tmp_data_path is None:
            tmp_data_path = './tmp.txt'
        if os.path.isfile(tmp_data_path):
            os.remove(tmp_data_path)
        for txt in tokenized:

            with open(tmp_data_path, 'a', encoding='utf-8') as f:
                f.write(" ".join(txt)+'\n')
        yttm.BPE.train(data=tmp_data_path, vocab_size=vocab_size, model=self.bpe_path,
                       pad_id=self.pad_ind, unk_id=self.unk_id, bos_id=self.bos_ind, eos_id=self.eos_ind)
        self.bpe = yttm.BPE(model=self.bpe_path)

    def encode(self, text: list):
        return self.bpe.encode(text, output_type=yttm.OutputType.ID, bos=True, eos=True)

    def decode(self, text: list):
        return self.bpe.decode(text)

    def word2ind(self, word):
        return self.bpe.subword_to_id(word)

    def ind2word(self, ind):
        return self.bpe.id_to_subword(ind)

    def vocab_size(self):
        return self.bpe.vocab_size()


def train_test_split(array, k):
    array = list(array)
    np.random.shuffle(array)
    n = int(len(array)*k)
    return array[:n], array[n:]


def batch_maker(src: List, trg: List, max_len_src: int = None, max_len_trg: int = None, pad: int = PAD_IND):
    if max_len_src is None:
        max_len_src = max([len(i) for i in src])
    if max_len_trg is None:
        max_len_trg = max([len(i) for i in trg])
    src_tensor = torch.empty(len(src), max_len_src)
    trg_tensor = torch.empty(len(trg), max_len_trg)
    for i in range(len(src)):
        src_tensor[i] = F.pad(torch.tensor(src[i]), (0, max_len_src - len(src[i])), value=pad)
        trg_tensor[i] = F.pad(torch.tensor(trg[i]), (0, max_len_trg - len(trg[i])), value=pad)
    return torch.tensor(src_tensor, dtype=torch.int64), torch.tensor(trg_tensor, dtype=torch.int64)


def batch_generator(data: Union[torch.tensor, np.array], batch_size: int,
                    max_len: int = None, pad: int = PAD_IND, shuffle: str = False):
    """
    Convert input text data to int before applying this functions
    :param data: input massive of int [[src_seq, trg_seq],..., [src_seq, trg_seq]]
    :param batch_size: size of batch
    :param max_len: maximum length of sequences
    :param pad: index (int) to pad empty ceil
    :param shuffle: shuffle data before sampling
    :return:
    """
    src, trg = data
    if shuffle:
        shfl_ind = np.arange(len(src))
        np.random.shuffle(shfl_ind)
    n_batches = len(src)//batch_size
    for i in range(n_batches):
        src_batch = src[i*batch_size: batch_size + i*batch_size]
        trg_batch = trg[i*batch_size: batch_size + i*batch_size]
        yield batch_maker(src_batch, trg_batch, max_len_src=max_len, max_len_trg=max_len, pad=pad)
