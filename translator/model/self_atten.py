"""
http://nlp.seas.harvard.edu/2018/04/03/attention.html

Encoder
- Embedding (word + pos embed) -> [batch_size, seq_len, embed_size]

Repeat this for N times:
    - Self attention (clone input embed in tree matrix (as query, key, values), apply multihead attention (split
    embedding dim in n equal heads) -> [batch_size, seq_len, embed_size]
    - ResidualConnection
    - apply FeedForward (linear net with relu)
    - ResidualConnection
    - Normalization

Decoder
- Embedding (word + pos embed) -> [batch_size, seq_len, embed_size]

Repeat this for N times:
    - Self attention (clone input embed in tree matrix (as query, key, values), apply multihead attention (split
        embedding dim in n equal heads) -> [batch_size, seq_len, embed_size]
    - Self attention (take value matrix from prev_layer (self_atten output vector) clone encoder output as query, key),
        apply multihead attention (split embedding dim in n equal heads) -> [batch_size, seq_len, embed_size]
    - ResidualConnection
    - apply FeedForward (linear net with relu)
    - ResidualConnection
    - Normalization
"""

import numpy as np
import torch
import torch.nn as nn
import torch.nn.functional as F
import math, copy, time
from torch.autograd import Variable
from translator.configs.vocab_configs import EOS_IND, BOS_IND, PAD_IND
import warnings
SEED = 1234

torch.manual_seed(SEED)
torch.cuda.manual_seed(SEED)
torch.backends.cudnn.deterministic = True


def clones(module, N):
    return nn.ModuleList([copy.deepcopy(module) for _ in range(N)])


def attention(query, key, value, mask=None, dropout=None):
    """
    attn = softmax( ( (query @ key.T)/sqrt(dim_key)) @ V)
    :param query: [batch_size, n_heads, seq_len, dim_head]
    :param key: [batch_size, n_heads, seq_len, dim_head]
    :param value: [batch_size, n_heads, seq_len, dim_head]
    :param mask: # [batch_size, 1, 1, seq_len]
    :param dropout:
    :return:
    """
    dim_head = query.size(-1)
    scores = torch.matmul(query, key.transpose(-2, -1))/math.sqrt(dim_head)  # [batch_size, n_heads, seq_len, seq_len]
    if mask is not None:
        scores = scores.masked_fill(mask == PAD_IND, -1e9)
    p_attn = F.softmax(scores, dim=-1)  # [batch_size, n_heads, seq_len, seq_len]
    if dropout is not None:
        p_attn = dropout(p_attn)
    attentioned_query = torch.matmul(p_attn, value)  # [batch_size, n_heads, seq_len, dim_head]
    return attentioned_query, p_attn


def knowing_mask_trg(trg):
    trg_mask = (trg != PAD_IND).unsqueeze(-2)
    attn_shape = (1, trg.size(-1), trg.size(-1))
    diag = torch.triu(torch.ones(attn_shape), diagonal=1) == 0
    return trg_mask&diag


def knowing_mask_src(src: torch.tensor):
    return (src != PAD_IND).unsqueeze(-2)


class MultiHeadedAttention(nn.Module):
    def __init__(self, n_heads, embed_dim, dropout=0.1):
        super(MultiHeadedAttention, self).__init__()
        assert embed_dim % n_heads == 0
        self.dim_head = embed_dim // n_heads
        self.n_head = n_heads
        self.q_matrix = nn.Linear(embed_dim, embed_dim)
        self.k_matrix = nn.Linear(embed_dim, embed_dim)
        self.v_matrix = nn.Linear(embed_dim, embed_dim)
        self.attn_linear = nn.Linear(embed_dim, embed_dim)
        self.attn = None
        self.dropout = nn.Dropout(p=dropout)

    def forward(self, query, key, value, mask=None):
        """
        :param query: [batch_size, seq_len, embed_size]
        :param key: [batch_size, seq_len, embed_size]
        :param value: [batch_size, seq_len, embed_size]
        :param mask: [batch_size, 1, seq_len]
        :return:
        """
        if mask is not None:  # Same mask applied to all h heads.
            mask = mask.unsqueeze(1)  # [batch_size, 1, 1, seq_len]
        n_batch = query.size(0)
        print('query size', query.shape)
        print('key size', key.shape)
        print('value size', value.shape)
        query, key, value = \
            [matrix(x).view(n_batch, -1, self.n_head, self.dim_head).transpose(1, 2)
             for matrix, x in zip(
                (self.q_matrix, self.k_matrix, self.v_matrix), (query, key, value)
            )]   # [batch_size, n_heads, seq_len, dim_head]

        x, self.attn = attention(
            query, key, value, mask=mask, dropout=self.dropout)  # [batch_size, n_heads, seq_len, dim_head]

        x = x.transpose(1, 2).contiguous().view(
            n_batch, -1, self.n_head * self.dim_head)  # [batch_size, seq_len, embed_dim]
        return self.attn_linear(x)


class FeedForward(nn.Module):
    def __init__(self, model_size, hide_dim, dropout=0.1):
        super(FeedForward, self).__init__()
        self.w_1 = nn.Linear(model_size, hide_dim)
        self.w_2 = nn.Linear(hide_dim, model_size)
        self.dropout = nn.Dropout(dropout)

    def forward(self, x):
        return self.w_2(self.dropout(F.relu(self.w_1(x))))


class LayerNorm(nn.Module):
    def __init__(self, n_feat: int, eps=1e-6):
        super(LayerNorm, self).__init__()
        self.a_2 = nn.Parameter(torch.ones(n_feat))
        self.b_2 = nn.Parameter(torch.zeros(n_feat))
        self.eps = eps

    def forward(self, x: torch.tensor):
        mean = x.mean(-1, keepdim=True)
        std = x.std(-1, keepdim=True)
        return self.a_2 * (x - mean) / (std + self.eps) + self.b_2


class ResConn(nn.Module):
    def __init__(self, size, dropout):
        super(ResConn, self).__init__()
        self.norm = LayerNorm(size)
        self.dropout = nn.Dropout(dropout)

    def forward(self, x, sublayer):
        return x + self.dropout(sublayer(self.norm(x)))


class Encoder(nn.Module):
    def __init__(self, layer, embed_size, N):
        super(Encoder, self).__init__()
        self.layers = clones(layer, N)
        self.norm = LayerNorm(embed_size)


    def forward(self, x, mask):
        """
        :param x: [batch_size, seq_len, embed]
        :param mask:
        :return:
        """
        for layer in self.layers:
            x = layer(x, mask)
        return self.norm(x)


class EncoderLayer(nn.Module):
    def __init__(self, embed_size, self_attn: MultiHeadedAttention, feed_forward: FeedForward, dropout: float):
        super(EncoderLayer, self).__init__()
        self.self_attn = self_attn
        self.feed_forward = feed_forward
        self.sublayer = clones(ResConn(embed_size, dropout), 2)

    def forward(self, x, mask):
        """
        :param x: [batch_size, seq_len, embed_size]
        :param mask:
        :return:
        """
        x = self.sublayer[0](x, lambda x: self.self_attn(x, x, x, mask))
        return self.sublayer[1](x, self.feed_forward)


class Decoder(nn.Module):
    def __init__(self, layer, embed_size, len_out_vocab, N):
        super(Decoder, self).__init__()
        self.layers = clones(layer, N)
        self.norm = LayerNorm(embed_size)
        self.output = nn.Linear(embed_size, len_out_vocab)

    def forward(self, x, memory, src_mask, trg_mask):
        for layer in self.layers:
            x = layer(x, memory, src_mask, trg_mask)
        prob_x = F.log_softmax(self.output(x), dim=-1)
        return prob_x


class DecoderLayer(nn.Module):
    def __init__(self, embed_size, self_attn, feed_forward, dropout):
        super(DecoderLayer, self).__init__()
        self.d_model = embed_size
        self.self_attn = clones(self_attn, 2)
        self.feed_forward = feed_forward
        self.sublayer = clones(ResConn(embed_size, dropout), 3)

    def forward(self, x, enc_output, src_mask, trg_mask):
        """
        :param x: [batch_size, seq_len, embed_size]
        :param enc_output: [batch_size, seq_len, embed_size]
        :param src_mask:
        :param trg_mask:
        :return:
        """
        x = self.sublayer[0](x, lambda x: self.self_attn[0](x, x, x, trg_mask))
        x = self.sublayer[1](x, lambda x: self.self_attn[1](x, enc_output, enc_output, src_mask))
        x = self.sublayer[2](x, self.feed_forward)
        return x


class EncoderDecoder(nn.Module):
    def __init__(self, encoder_layers: EncoderLayer, n_encoder_layers: int, d_model: int, src_vocab_size: int,
                 decoder_layers: DecoderLayer, n_decoder_layers: int, trg_vocab_size: int):
        super(EncoderDecoder, self).__init__()
        self.d_model = d_model
        self.encoder = Encoder(encoder_layers, d_model, n_encoder_layers)
        self.decoder = Decoder(decoder_layers, d_model, trg_vocab_size, n_decoder_layers)
        self.src_embed = nn.Embedding(src_vocab_size, d_model)
        self.trg_embed = nn.Embedding(trg_vocab_size, d_model)

    def forward(self, src: torch.tensor, trg: torch.tensor, teaching_rate=None):
        """
        :param src: [batch_size, len_src]
        :param trg: [batch_size, len_trg]
        :return:
        """
        src_mask = knowing_mask_src(src)
        trg_mask = knowing_mask_trg(trg)
        enc = self.encode(src, src_mask)
        dec = self.decode(enc, src_mask, trg, trg_mask)
        return dec

    def encode(self, src, src_mask):
        src_embed = self.positional_decoding(x=src, embed=self.src_embed)  # [batch_size, seq_len, embed]
        return self.encoder(src_embed, src_mask)

    def decode(self, prev_trg_tokens, src_mask, trg, trg_mask):
        trg_embed = self.positional_decoding(x=trg, embed=self.trg_embed)
        return self.decoder(trg_embed, prev_trg_tokens, src_mask, trg_mask)

    @staticmethod
    def positional_decoding(x: torch.tensor, embed: nn.Embedding):
        x = embed(x)  # [batch_size, seq_len, embed_size]
        embed_size = x.shape[-1]
        x *= math.sqrt(embed_size)
        seq_len = x.shape[1]
        pe = torch.zeros(seq_len, embed_size)
        position = torch.arange(0, seq_len).unsqueeze(1)
        div_term = torch.exp(torch.arange(0, embed_size, 2) * -(math.log(10000.0) / embed_size))
        print('__________________________')
        print(pe.shape)
        pe[:, 0::2] = torch.sin(position * div_term)
        pe[:, 1::2] = torch.cos(position * div_term)
        pe = pe.unsqueeze(0)
        return x + pe

    def translate_lines(self, src, out_voc, max_len=None, device=None):
        if max_len is None:
            max_len = int(1.3*src.shape[1])
        src_mask = knowing_mask_src(src)
        memory = self.encode(src=src, src_mask=src_mask)
        trg_y = torch.full([src.shape[0], 1], 0).to(device=src.device)
        for i in range(max_len - 1):
            trg_mask = knowing_mask_trg(trg_y)
            out = self.decode(memory, src_mask, trg_y, trg_mask)[:, -1]
            _, next_word = torch.max(out, dim=1)
            next_word = next_word.data[0]
            trg_y = torch.cat([trg_y, torch.ones(1, 1).type_as(src.data).fill_(next_word)], dim=1)
        return out_voc.to_lines(trg_y.cpu().numpy())


def construct_nlp_monster(src_vocab, trg_vocab, n_layers: int = 6, embed_size: int = 512, hid_size: int = 2048,
                          n_heads: int = 8, dropout: float = 0.6):
    c = copy.deepcopy
    attn = MultiHeadedAttention(n_heads, embed_size)
    ff = FeedForward(embed_size, hid_size, dropout)
    enc_layer = EncoderLayer(embed_size=embed_size, self_attn=c(attn), feed_forward=c(ff), dropout=dropout)
    dec_layer = DecoderLayer(embed_size=embed_size, self_attn=c(attn), feed_forward=c(ff), dropout=dropout)
    model = EncoderDecoder(encoder_layers=enc_layer, n_encoder_layers=n_layers, d_model=embed_size,
                           src_vocab_size=src_vocab.vocab_size(), decoder_layers=dec_layer, n_decoder_layers=n_layers,
                           trg_vocab_size=trg_vocab.vocab_size())

    for p in model.parameters():
        if p.dim() > 1:
            nn.init.xavier_uniform(p)
    return model

#
# sku_transform_dict = model_coef[model_coef[ModelCoefs.feature] == col][
#     [ModelsMetaInfo.sku_id, ModelCoefs.transform]]
# print('transformed')
# print(df_promo[f"{col}_t"])
# df_promo[f"{col}_t"] = df_promo[[ModelsMetaInfo.sku_id, f"{col}"]].apply(
#     lambda x: getattr(transformations, col)(x[1]))