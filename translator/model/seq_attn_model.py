import numpy as np
import torch
import torch.nn as nn
import torch.nn.functional as F
from translator.configs.vocab_configs import EOS_IND, BOS_IND
import warnings
SEED = 1234

torch.manual_seed(SEED)
torch.cuda.manual_seed(SEED)
torch.backends.cudnn.deterministic = True

warnings.filterwarnings('ignore')


class AttentionLayer(nn.Module):
    def __init__(self, enc_size, dec_size, hid_size, activ=torch.tanh):
        """
        A layer that computes additive attention response and weights

        similarity = v.T * tanh( W.T * concat(enc_output, prev_hid_state_dec) [batch_size x len_seq]
        probs = softmax(similarity) [batch_size x len_seq]
        attn = probs @ enc [batch_size x enc_size]
        """
        super().__init__()
        self.enc_size = enc_size  # num units in encoder state
        self.dec_size = dec_size  # num units in decoder state
        self.hid_size = hid_size  # attention layer hidden units
        self.activ = activ  # attention layer hidden nonlinearity
        self.lin_w = nn.Linear(enc_size + dec_size, hid_size)
        self.lin_v = nn.Linear(hid_size, 1)
        self.lin_attn = nn.Linear(hid_size, hid_size)

    def forward(self, enc, dec, inp_mask):
        """
        Computes attention response and weights
        :param enc: encoder activation sequence, float32[batch_size, len_seq, enc_size]
        :param dec: single decoder state used as "query", float32[batch_size, dec_size]
        :param inp_mask: mask on enc activatons (0 after first eos), float32 [batch_size, len_seq]
        :returns: attn[batch_size, enc_size], probs[batch_size, len_seq]
            - attn - attention response vector (weighted sum of enc)
            - probs - attention weights after softmax
        """
        dec = dec[:, None, :]
        dec_enc = torch.cat((dec.expand(-1, enc.shape[1], -1), enc), -1) # [batch_size, len_seq, enc_size+dec_size]
        logits = self.lin_v(self.activ(self.lin_w(dec_enc))).squeeze(-1) # [batch_size, len_seq]
        inp_mask = torch.log(inp_mask) + 1  # [batch_size, len_seq]
        inp_mask[inp_mask < 0] = -1e-9
        masked_logits = logits * inp_mask   # [batch_size, len_seq] true_logits if mask=1 otherwise -1e9
        # Compute attention probabilities (softmax)
        probs = torch.softmax(masked_logits, -1)
        # Compute attention response using enc and probs
        attn = torch.bmm(probs[:, None, :], enc).squeeze(1)
        return attn, probs


class AttentiveModel(nn.Module):
    def __init__(self, inp_voc_len, out_voc_len, max_length, enc_size=128, dec_size=128,
                 emb_size=64, hid_size=128, attn_size=128):
        """
        SeqtoSeq model with attention layer
        GRU(enc) -> |__ Attn          |   GRUCell(concat(attn, prev_hid), prev_token) |
                    |__ Prev_Hidden   |->                                             |
                    |__ Prev_token    |                                               |
                    ^_________________________________________________________________|
        :param inp_voc:
        :param out_voc:
        :param enc_size:
        :param dec_size:
        :param emb_size:
        :param hid_size:
        :param attn_size:
        """
        nn.Module.__init__(self)
        self.inp_voc_len, self.out_voc_len = inp_voc_len, out_voc_len
        self.hid_size = hid_size
        self.attn = AttentionLayer(enc_size, dec_size, attn_size)

        self.emb_inp = nn.Embedding(self.inp_voc_len, emb_size)
        self.emb_out = nn.Embedding(self.out_voc_len, emb_size)
        self.enc0 = nn.GRU(emb_size, enc_size, batch_first=True)

        self.dec_start = nn.Linear(enc_size, dec_size)
        self.dec0 = nn.GRUCell(emb_size, enc_size + dec_size)
        self.dec_lin = nn.Linear(enc_size + dec_size, dec_size)
        self.output = nn.Linear(dec_size, self.out_voc_len)
        self.max_length = max_length

    @staticmethod
    def cheating(teacher_force):
        return bool(np.random.binomial(1, teacher_force)>0)

    def encode(self, inp, **flags):
        """
        Encoding input seq, count attn layer from first hidden decoder state (last state of encoding seq)
        and encoding input seq
        :param inp: [batcg_size, seq_len]
        :param flags:
        :return: first hidden decoder state, encoding seq, attn, attention weights, mask of input seq
        """
        # encode input sequence, create initial decoder states
        inp_emb = self.emb_inp(inp)
        enc_seq, _ = self.enc0(inp_emb)  # enc_seq: [batch, time, hid_size], _: [batch, hid_size]

        lengths = (inp != EOS_IND).to(torch.int64).sum(dim=1).clamp_max(inp.shape[1] - 1)
        last_state = enc_seq[torch.arange(len(enc_seq)), lengths]  # [batch_size, hid_size]

        dec_start = self.dec_start(last_state)  # linear for last_state [batch, hid_size]
        inp_mask = F.pad(torch.cumsum(inp == EOS_IND, dim=-1)[..., :-1] < 1, pad=(1, 0, 0, 0), value=True)
        # apply attention layer from initial decoder hidden state
        attn, probs = self.attn(enc_seq, dec_start, inp_mask)  # attn: [batch, hid_size], probs: [batch, len_seq]
        return [[dec_start], enc_seq, attn, probs, inp_mask]

    def decode_step(self, prev_state, attn, prev_tokens):
        """
        Takes previous decoder state, attn and tokens, returns new state and logits for next tokens
        :param prev_state: [batch_size, hid_size (enc + dec)]
        :param prev_tokens: previous output tokens, an int vector of [batch_size]
        :return: a list of next decoder state tensors, a tensor of logits [batch, len(out_voc)]
        """
        state_with_attn = torch.cat((prev_state, attn), dim=-1)  # [batch_size, enc_size + dec_size]
        emb = self.emb_out(prev_tokens).squeeze(1)  # [batch_size x emb_size]
        new_dec_state = self.dec_lin(self.dec0(emb, state_with_attn))  # [batch_size x enc_size + dec_size]
        output = self.output(new_dec_state)  # [batch_size x len(out_voc)]
        return new_dec_state, output

    def decode(self, enc_seq, initial_state, attn, probs, mask, out_tokens: torch.tensor = None,
               teacher_rate: float = 0, **flags):
        """
        Iterate over out_tokens. Each iteration:
        1) count attn from previous hidden state and enc_seq
        2) concat attn and previous hidden state as attn_hid
        3) decoding token using att_hid and previous token
        :param enc_seq: [batch_size, seq_len, hid_size]
        :param initial_state: [batch_size, hid_size]
        :param attn: [batch, hid_size]
        :param probs: [batch, seq_len]
        :param out_tokens: [batch_size, out_seq_len]
        :param mask: [batch_size, inp_seq_len]
        :param flags:
        :return:
        """
        state = initial_state[0]
        device = enc_seq.device
        onehot_bos = F.one_hot(torch.full([enc_seq.shape[0]], BOS_IND, dtype=torch.int64),
                               num_classes=self.out_voc_len).to(device=device)
        logits_sequence = [onehot_bos]
        prev_token = torch.full([enc_seq.shape[0], 1], BOS_IND).to(device=device)
        self.max_length = enc_seq.shape[1] + 10

        def decode_loop(prev_stat, encod_attn, prev_tokens):
            states, dec_logits = self.decode_step(prev_state=prev_stat, attn=encod_attn, prev_tokens=prev_tokens)
            logits_sequence.append(dec_logits)
            _, prev_tokens = dec_logits.topk(1)
            encod_attn, probabilities = self.attn(enc_seq, state, mask)
            return state, attn, prev_tokens, probabilities

        i = 0
        if out_tokens is not None:
            self.max_length = out_tokens.shape[1]
            while i != self.max_length-1:
                state, attn, prev_token, probas = decode_loop(state, attn, prev_token)
                if self.cheating(teacher_rate):
                    prev_token = torch.tensor(out_tokens[:, i+1])
                i += 1
        else:
            while (i != self.max_length+1) and (prev_token != EOS_IND):
                state, attn, prev_token, probas = decode_loop(state, attn, prev_token)
                i += 1
        return torch.stack(logits_sequence, dim=1)

    def forward(self, inp: torch.tensor, out: torch.tensor = None, teacher_rate: float = 0):
        dec_start, enc_seq, attn, probs, inp_mask = self.encode(inp)
        result = self.decode(enc_seq=enc_seq, initial_state=dec_start, attn=attn, probs=probs,
                            out_tokens=out, mask=inp_mask, teacher_rate=teacher_rate)
        return result


    def translate_lines(self, inp, out_voc, device, **kwargs):
        # inp = self.inp_voc.to_matrix(inp_lines).to(device)
        out_ids = self.forward(inp)
        return out_voc.to_lines(out_ids.argmax(dim=-1).cpu().numpy())