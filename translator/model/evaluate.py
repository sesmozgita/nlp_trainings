import numpy as np
from IPython.display import clear_output
from tqdm import tqdm, trange
import torch
import torch.nn.functional as F
from nltk.translate.bleu_score import corpus_bleu
from translator.model.seq_attn_model import AttentiveModel
from translator.data.data_preparation import batch_generator
from translator.configs.vocab_configs import EOS_IND, BOS_IND, PAD_IND, HD
from translator.data.vocab import Vocab
from torch.optim.lr_scheduler import ReduceLROnPlateau
import matplotlib.pyplot as plt


class TranslatorLossCounter:
    def __init__(self, out_voc_len):
        self.out_voc_len = out_voc_len

    def target_extract(self, data):
        target_one_hot = F.one_hot(data, self.out_voc_len).to(torch.float32)  # [batch_size, len_seq, n_out_tokens)]
        return target_one_hot

    def compute_loss(self, target: torch.tensor, predict: torch.tensor, mask: torch.tensor):
        target = self.target_extract(target)
        logprobs_seq = torch.log_softmax(predict, dim=-1)  # [batch_size, len_seq, len(out_voc)]
        logp_out = (logprobs_seq * target).sum(dim=-1)  # [batch_size, len_seq]
        logp_out = - torch.nansum(logp_out * mask)/(torch.sum(mask))
        return logp_out


def compute_bleu(model: AttentiveModel, predicts: torch.tensor, target: torch.tensor, trg_vocab: Vocab,
                 bpe_sep: str = '▁', **flags):
    with torch.no_grad():
        str_target = trg_vocab.to_lines(target.cpu().numpy())
        translations = model.translate_lines(predicts.argmax(-1), out_voc=trg_vocab, device=predicts.device, **flags)
        translations = [line.replace(bpe_sep, '') for line in translations]
        actual = [line.replace(bpe_sep, '') for line in str_target]
        return corpus_bleu(
            [[ref.split()] for ref in actual],
            [trans.split() for trans in translations],
            smoothing_function=lambda precisions, **kw: [p + 1.0 / p.denominator for p in precisions]
            ) * 100


def training_loop(model, optimizer, train_data, test_data, inp_voc, out_voc, logger, **batch_kwargs):
    device = "cuda" if torch.cuda.is_available() else "cpu"
    loss = TranslatorLossCounter(out_voc_len=out_voc.vocab_size())
    best_score = np.inf
    n_epoches = 100
    start_teacher_rates = 0.8
    teacher_rates = list(
        1-np.geomspace((1-start_teacher_rates), start_teacher_rates, n_epoches, endpoint=True))

    for epoch in range(n_epoches):
        teacher_rate = teacher_rates[epoch]
        v_history = []
        v_bleu = []
        t_history = []
        for src, trg in batch_generator(data=train_data, **batch_kwargs):
            model.train()
            optimizer.zero_grad()
            mask = out_voc.compute_mask(trg)
            predicts = model(src, trg, max(0.6, teacher_rate))
            loss_t = loss.compute_loss(predict=predicts, target=trg, mask=mask)
            loss_t.backward()
            optimizer.step()
            t_history.append(loss_t.item())

        for src, trg in batch_generator(data=test_data, **batch_kwargs):
            model.eval()
            mask = out_voc.compute_mask(trg)
            predicts = model(src, trg)
            loss_v = loss.compute_loss(predict=predicts, target=trg, mask=mask)
            v_history.append(loss_v.item())
            v_bleu.append((epoch, compute_bleu(model, predicts=predicts, target=trg, trg_vocab=out_voc)))

        checkpoint = {
            'epoch': epoch + 1,
            'state_dict': model.state_dict(),
            'optimizer': optimizer.state_dict()
        }
        if best_score > np.mean(v_history):
            best_score: float = np.mean(v_history)
            acc = "".join(f"{round(best_score, 2)}".split('.'))
            torch.save(checkpoint, f'translate_model.pt')

        if epoch % 10 == 0:
            logger.info(
                f"Epoch: {epoch} | Mean loss train: {np.mean(t_history)} | | Mean loss valid: {np.mean(v_history)}")
    logger.info("I am end")
    return model


def plot_training_loop(model, out_voc, opt, train_data, test_data, **batch_kwargs):
    loss = TranslatorLossCounter(out_voc=out_voc)
    generator_train = batch_generator(data=train_data, **batch_kwargs)
    generator_test = batch_generator(data=test_data, **batch_kwargs)
    metrics = {'train_loss': [], 'dev_bleu': []}
    for _ in trange(25000):
        src, trg = generator_train
        dev_inp, dev_out = generator_test
        step = len(metrics['train_loss']) + 1

        opt.zero_grad()
        loss_t = loss.compute_loss(src, trg)
        loss_t.backward()
        opt.step()

        metrics['train_loss'].append((step, loss_t.item()))
        if step % 100 == 0:
            metrics['dev_bleu'].append((step, compute_bleu(model, dev_inp, dev_out)))

            clear_output(True)
            plt.figure(figsize=(12, 4))
            for i, (name, history) in enumerate(sorted(metrics.items())):
                plt.subplot(1, len(metrics), i + 1)
                plt.title(name)
                plt.plot(*zip(*history))
                plt.grid()
            plt.show()
            print("Mean loss=%.3f" % np.mean(metrics['train_loss'][-10:], axis=0)[1], flush=True)
