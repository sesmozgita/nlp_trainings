import unittest
import torch
from translator.model.seq_attn_model import AttentiveModel
from translator.data.vocab import Vocab
torch.manual_seed(43)
train_inp = ['i make the awesome translator', 'the best way to make me happy: increase BLEU']
train_out = ['я делаю офигенный переводчик', 'лучший способ сделать меня счастливой: повысить BLEU']
inp_voc = Vocab.from_lines(train_inp)
out_voc = Vocab.from_lines(train_out)
m = AttentiveModel(inp_voc=inp_voc, out_voc=out_voc)

inp = torch.tensor([[0, 4, 5, 6, 1]], dtype=torch.int64)
out = torch.tensor([[0, 4, 5, 6, 1]], dtype=torch.int64)


class MyTestCase(unittest.TestCase):
    # def test_basic(self):
    #     outputs = m(inp, out)
    #     list_shape = list(inp.shape) + [len(out_voc)]
    #     self.assertListEqual(list_shape, list(outputs.shape))
    #
    #     probs_of_bos = outputs[0][:, 0].argmax()
    #     self.assertEqual(int(probs_of_bos), 0)

    # def test_encode(self):
        # batch_size = 1
        # len_seq = 5
        # enc_size = 32
        # dec_size = 64
        # hid_size = 128
        # attn_size = 256
        # model = AttentiveModel(inp_voc=inp_voc, out_voc=out_voc, enc_size=enc_size, dec_size=dec_size,
        #                        hid_size=hid_size, attn_size=attn_size)
        #
        # dec_start, enc_seq, attn, probs, inp_mask = model.encode(inp)
        # self.assertEqual(len(dec_start), 1)  # dec_start is initial hidden state for decoder
        # self.assertEqual(list(dec_start[0].shape), [batch_size, dec_size])
        # self.assertEqual(list(enc_seq.shape), [batch_size, len_seq, enc_size])
        # self.assertEqual(list(probs.shape), [batch_size, len_seq])
        # self.assertEqual(list(inp_mask.shape), [batch_size, len_seq])

    # def test_decode_step(self):
    #     batch_size = 1
    #     len_seq = 5
    #     enc_size = 32
    #     dec_size = 64
    #     hid_size = 128
    #     attn_size = 256
    #     model = AttentiveModel(inp_voc=inp_voc, out_voc=out_voc, enc_size=enc_size, dec_size=dec_size,
    #                            hid_size=hid_size, attn_size=attn_size)
    #     dec_start, enc_seq, attn, probs, inp_mask = model.encode(inp)
    #     new_dec_state, output = model.decode_step(prev_state=dec_start[0], attn=attn, prev_tokens=out[:,0])
    #     self.assertEqual(list(new_dec_state.shape), [batch_size, dec_size])
    #     self.assertEqual(list(output.shape), [batch_size, len(out_voc)])

    # def test_decode(self):
    #     batch_size = 1
    #     len_seq = 5
    #     enc_size = 32
    #     dec_size = 64
    #     hid_size = 128
    #     attn_size = 256
    #     model = AttentiveModel(inp_voc=inp_voc, out_voc=out_voc, enc_size=enc_size, dec_size=dec_size,
    #                            hid_size=hid_size, attn_size=attn_size)
    #     dec_start, enc_seq, attn, probs, inp_mask = model.encode(inp)
    #     output = model.decode(enc_seq=enc_seq, initial_state=dec_start, attn=attn,
    #                                          probs=probs, out_tokens=out, mask=inp_mask)
    #     self.assertEqual(list(output.shape), [batch_size, len_seq, len(out_voc)])

    def test_translate_lines(self):
        batch_size = 1
        len_seq = 5
        enc_size = 32
        dec_size = 64
        hid_size = 128
        attn_size = 256
        model = AttentiveModel(inp_voc=inp_voc, out_voc=out_voc, enc_size=enc_size, dec_size=dec_size,
                               hid_size=hid_size, attn_size=attn_size)
        translated_lines = model.translate_lines(train_inp, device="cpu", max_len=5)
        print(translated_lines)


if __name__ == '__main__':
    unittest.main()

