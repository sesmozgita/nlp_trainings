import unittest
import torch
from translator.data.data_preparation import SimpleVocab, YttmVocab
from translator.model.seq_attn_model import AttentiveModel
from translator.data.vocab import Vocab
torch.manual_seed(42)


train_inp = ['i make the awesome translator', 'the best way to make me happy: increase BLEU']
train_out = ['я делаю офигенный переводчик', 'лучший способ сделать меня счастливой: повысить BLEU']
inp_voc = Vocab.from_lines(train_inp)
out_voc = Vocab.from_lines(train_out)
m = AttentiveModel(inp_voc=inp_voc, out_voc=out_voc)

inp = torch.tensor([[0, 4, 5, 6, 1]], dtype=torch.int64)
out = torch.tensor([[0, 4, 5, 6, 1]], dtype=torch.int64)


class MyTestCase(unittest.TestCase):
    def test_simple_vocab(self):
        train_vocab = SimpleVocab()
        tokenized_txt = train_vocab.tokenize(train_inp[1])
        self.assertListEqual(['the', 'best', 'way', 'to', 'make', 'me', 'happy', ':', 'increase', 'BLEU']
                             , tokenized_txt)
        train_vocab.vocab_build(train_inp, 3)
        word2ind = train_vocab.word2ind
        ind2word = train_vocab.ind2word
        real_dict = {'_bos_': 0, '_eos_': 1, '_unk_': 2, '_pad_': 3, 'make': 4, 'the': 5, 'i': 6}
        self.assertDictEqual(word2ind, real_dict)
        self.assertDictEqual(ind2word, {v: k for k, v in real_dict.items()})


