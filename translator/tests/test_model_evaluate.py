import unittest
import torch
import torch.nn as nn
from translator.model.evaluate import TranslatorLossCounter
from translator.model.seq_attn_model import AttentiveModel
from translator.data.vocab import Vocab
import numpy as np
import numpy.testing as npt
torch.manual_seed(42)


train_inp = ['i make the awesome translator', 'the best way to make me happy: increase BLEU']
train_out = ['я делаю офигенный переводчик', 'лучший способ сделать меня счастливой: повысить BLEU']
inp_voc = Vocab.from_lines(train_inp)
out_voc = Vocab.from_lines(train_out)
m = AttentiveModel(inp_voc=inp_voc, out_voc=out_voc)

inp = torch.tensor([[0, 4, 5, 6, 1]], dtype=torch.int64)
out = torch.tensor([[0, 4, 5, 6, 1]], dtype=torch.int64)

class Model(nn.Module):
    def __init__(self, n_voc, embed_dim):
        super(Model, self).__init__()
        self.emb = nn.Embedding(n_voc, embed_dim)
        self.lin = nn.Linear(embed_dim, n_voc)

    def forward(self, x):
        emb = self.emb(x)
        return self.lin(emb)


class MyTestCase(unittest.TestCase):
    def test_basic(self):
        embed_dim = 128
        model = Model(len(out_voc), embed_dim)
        x = model(inp)
        expected = np.array([[
            [0.3985, -1.0310, -0.5027, -0.0543, -0.4442, -0.2702,  0.1029, 0.5975,
              -0.6382, -0.3120,  0.5249, -0.2268,  0.0108, -0.1928],
            [0.7713, -0.3752, -0.7924, -0.0993, -1.3544,  0.1639,  0.1301, -0.6859,
              -0.1816, -0.1588,  0.3300, -0.0343,  0.4295,  0.9220],
            [0.3922, -0.0110,  0.7193,  0.9129,  0.3613, -0.2254,  1.0087, 1.1713,
             -0.5669, -0.3470,  0.0332, -0.6094, -0.6951,  0.1927],
            [0.3529,  0.5493, -0.1036,  0.5292,  0.1627, -0.4199,  0.5531, -0.5628,
              -0.3658,  0.1579,  0.0214, -0.9276,  0.5400, -0.9623],
            [0.3307, -0.0336, -0.0656, -0.3050,  0.0108, -0.3327,  1.7285, -0.1814,
              -0.7720, -0.4948, -0.4884,  0.8115, -0.3197, -0.2137]]])
        npt.assert_almost_equal(np.around(x.detach().numpy(), 4), expected, decimal=1)

        loss = TranslatorLossCounter(model, out_voc)

        error = loss.compute_loss(inp, out)

        softmax_pred = torch.softmax(x, -1)
        log_pred = torch.log(softmax_pred)[0]
        real_loss = -torch.tensor([log_pred[0][out[0][0]], log_pred[1][out[0][1]], log_pred[2][out[0][2]],
                                          log_pred[3][out[0][3]], log_pred[4][out[0][4]]]).mean()

        torch.testing.assert_close(error, real_loss, check_device=False)
