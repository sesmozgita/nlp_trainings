import unittest
from translator.model.self_atten import EncoderDecoder, construct_nlp_monster
from translator.data.data_preparation import SimpleVocab
import torch

torch.manual_seed(0)
data = ['я вас любил.', 'любовь еще быть может', 'в душей моей', 'угасла не совсем']
src_vocab = SimpleVocab()
src_vocab.vocab_build(data, vocab_size=10)

data = ['i loved you.', 'my love is probably', 'in my soul', 'have not burnt out yet']
trg_vocab = SimpleVocab()
trg_vocab.vocab_build(data, vocab_size=10)

model = construct_nlp_monster(src_vocab, trg_vocab, dropout=0)


class MyTestCase(unittest.TestCase):
    def test_something(self):
        src = torch.tensor([[1, 2, 3, 4]])
        trg = torch.tensor([[1, 2, 3, 4]])
        pred_sum = model(src, trg).sum()
        self.assertAlmostEqual(pred_sum.item(), -425.6082, delta=1e-3)

        encoded = model.encode(src, torch.tensor([1, 0, 0, 0]))
        print(encoded.sum().item())

        encoded = model.encode(src, torch.tensor([1, 1, 0, 0]))
        print(encoded.sum().item())




if __name__ == '__main__':
    unittest.main()
