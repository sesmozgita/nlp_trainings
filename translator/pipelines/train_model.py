from typing import List
import torch
import numpy as np

from translator.configs.path_config import *
from translator.pipelines.vocab_train import vocab_prep
from translator.model.seq_attn_model import AttentiveModel
from translator.data.data_preparation import add_spec
from translator.model.evaluate import training_loop
from utils import logger_setup, pickle_loading, pickle_dumping

emb_size = 256
hid_size_conv = 512
hid_size_gru = 512
vocab_size = 20000
batch_kwargs = {"batch_size": 1}
logger = logger_setup("./logs/train_translate_model.log")
src_vocab = vocab_prep(path_to_sent=src_data_path, path_to_bpe_mdl=bpe_model_src_path, path_to_vocab=src_vocab_path,
                       vocab_size=vocab_size, tmp_data_path='src_tmp.txt')
trg_vocab = vocab_prep(path_to_sent=trg_data_path, path_to_bpe_mdl=bpe_model_trg_path, path_to_vocab=trg_vocab_path,
                       vocab_size=vocab_size, tmp_data_path='trg_tmp.txt')


if __name__ == "__main__":

    data_inp: List[str] = list(open(src_data_path, encoding='utf-8').read().lower().split('\n')) # list of sentences
    data_out: List[str] = list(open(trg_data_path, encoding='utf-8').read().lower().split('\n'))  # list of sentences
    tr_ind_board = int(0.9*len(data_inp))
    src_tr, trg_tr = src_vocab.to_matrix(data_inp)[: tr_ind_board], trg_vocab.to_matrix(data_out)[:tr_ind_board]
    src_tst, trg_tst = src_vocab.to_matrix(data_inp)[tr_ind_board:], trg_vocab.to_matrix(data_out)[tr_ind_board:]
    model = AttentiveModel(src_vocab.vocab_size(), trg_vocab.vocab_size(), max_length=100,
                           enc_size=128, dec_size=128, emb_size=64, hid_size=128, attn_size=128)
    optimizer = torch.optim.Adam(model.parameters())
    model: AttentiveModel = training_loop(model=model, optimizer=optimizer, train_data=[src_tr, trg_tr],
                  test_data=[src_tst, trg_tst], inp_voc=src_vocab, out_voc=trg_vocab, logger=logger, **batch_kwargs)