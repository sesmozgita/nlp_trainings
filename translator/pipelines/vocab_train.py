"""
Use data from http://www.statmt.org/wmt19/translation-task.html#download (Yandex Corpus)
Unzip downloading data to src_data_path and trg_data_path
"""
from typing import List

from translator.data.data_preparation import YttmVocab, add_spec
from translator.configs.path_config import *
from utils import pickle_dumping

vocab_size = 20000


def vocab_prep(path_to_sent: str, path_to_bpe_mdl: str, path_to_vocab: str, vocab_size: int, encoding: str ='utf-8',
               tmp_data_path='./tmp.txt'):
    data: List[str] = list(open(path_to_sent, encoding=encoding).read().lower().split('\n'))
    voc = YttmVocab(bpe_path=path_to_bpe_mdl)
    voc.vocab_build(data, vocab_size=vocab_size, tmp_data_path=tmp_data_path)
    return voc


if __name__ == "__main__":
    vocab_prep(path_to_sent=src_data_path, path_to_bpe_mdl=bpe_model_src_path, path_to_vocab=src_vocab_path,
               vocab_size=vocab_size)
    vocab_prep(path_to_sent=trg_data_path, path_to_bpe_mdl=bpe_model_trg_path, path_to_vocab=trg_vocab_path,
               vocab_size=vocab_size)


