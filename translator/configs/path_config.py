src_data_path = "./translator/corpus.en_ru.poetry.ru"
trg_data_path = "./translator/corpus.en_ru.poetry.en"
# src_data_path = "./translator/train_ru.txt"
# trg_data_path = "./translator/train_en.txt"

# src_data_path = "./translator/corpus.en_ru.01m.ru"
# trg_data_path = "./translator/corpus.en_ru.01m.en"

bpe_model_src_path = "./translator/train_bpe.ru"
bpe_model_trg_path = "./translator/train_bpe.en"

src_vocab_path = './translator/source_vocab.pickle'
trg_vocab_path = './translator/target_vocab.pickle'
