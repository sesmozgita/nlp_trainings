import logging
import time
from datetime import date, datetime
import os
import re
import pickle


def path_join(path1, *path2):
    return os.path.join(path1, *path2)


def pickle_dumping(object_, *path):
    end_path = path_join(*path)
    with open(end_path, "wb") as f:
        pickle.dump(object_, f)

def pickle_loading(*path):
    end_path = path_join(*path)
    with open(end_path, "rb") as f:
        obj = pickle.load(f)
    return obj


def logger_setup(path: str):
    logger = logging.getLogger(__name__)
    logger.setLevel(logging.INFO)
    formatter = logging.Formatter("%(asctime)s %(name)-12s %(levelname)-8s %(message)s")
    filehandler = logging.FileHandler(path)
    filehandler.setLevel(logging.INFO)
    filehandler.setFormatter(formatter)
    logger.addHandler(filehandler)
    return logger

def log_time(logger):
    def decorator(method):
        def timed(*args, **kwargs):
            ts = time.time()
            result = method(*args, **kwargs)
            te = time.time()
            logger.info(f"time of {method.__name__} executing is {te-ts}")
            return result
        return timed
    return decorator


def from_str_to_dt(string: str, frmt: str = '%Y-%m-%d'):
  return datetime.strptime(string, frmt)

def from_dt_to_str(dt: datetime, frmt: str = '%Y-%m-%d'):
  return dt.strftime(frmt)

def clean_list(array: list, string: str):
    """
    remove specific string from list of sting's
    :param array: list
    :param string: string
    :return:
    """
    copy_list = array.copy()
    if string in copy_list:
        copy_list.remove(string)
    return copy_list


def extract_col(array: list, reg_exp: str):
    return [col for col in array if re.search(reg_exp, col)]


def write_report(path_file: str, mode: str = "w", message: str = "Report"):
    with open(path_file, mode) as f:
        f.write(f"{message}\n")


def get_from_dict( x,vocab, returned):
    return vocab.get(x, returned)